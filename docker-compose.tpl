version: '3'

services:
  db:
    image: postgres
    volumes:
      - ./postgres-data:/var/lib/postgresql/data
    ports:
     - $$external_psql_port$$:$$interal_psql_port$$

  web:
    build:
      context: .
      args:
        - REQUIREMENTS_FILE=$$requirements_file$$
    entrypoint: /entrypoint.sh
    command: python manage.py runserver 0.0.0.0:$$internal_backend_port$$
    volumes:
      - .:/code
    ports:
      - $$external_backed_port$$:$$internal_backend_port$$
    depends_on:
      - db

    # To facilitate debugging (pdb) in running container
    stdin_open: true
    tty: true

  pgadmin:
    image: thajeztah/pgadmin4
    depends_on:
      - db
    ports:
      - $$external_db_admin_port$$:$$internal_db_admin_port$$
    environment:
      PGADMIN_DEFAULT_EMAIL: pgadmin4@pgadmin.org
      PGADMIN_DEFAULT_PASSWORD: admin
    restart: unless-stopped