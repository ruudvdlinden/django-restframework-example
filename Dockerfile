FROM python:3

ARG REQUIREMENTS_FILE
ENV PYTHONUNBUFFERED 1

RUN mkdir /code
WORKDIR /code
COPY . /code/
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
RUN pip install -r ${REQUIREMENTS_FILE} --no-cache
