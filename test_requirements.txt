-r requirements.txt
flake8==3.7.7
mypy==0.711
pytest==4.5.0
pytest-django==3.4.8
readline==6.2.4.1
