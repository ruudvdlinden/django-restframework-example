import json

from django.test import TestCase
from rest_framework.test import APIClient


class TestClient(APIClient):
    @staticmethod
    def _enforce_request_format(keyword_arguments):
        expected_format = 'application/json'

        keyword_arguments['content_type'] = keyword_arguments.get('content_type', expected_format)

        if keyword_arguments['content_type'] != expected_format:
            raise ValueError('This client enforces request format "application/json"')

        return keyword_arguments

    @staticmethod
    def _stringify_data(keyword_arguments):
        """Converts request data to a JSON stringification, and tests if a string is from valid JSON"""

        if not keyword_arguments.get('data'):
            raise ValueError('Use a keyword argument for request data')

        if isinstance(keyword_arguments['data'], dict):
            keyword_arguments['data'] = json.dumps(keyword_arguments['data'])

        try:
            json.loads(keyword_arguments['data'])
        except Exception:
            raise TypeError('Supplied data is not a dict, or valid JSON string')

        return keyword_arguments

    def post(self, *args, **kwargs):
        kwargs = self._enforce_request_format(kwargs)
        kwargs = self._stringify_data(kwargs)

        return super(TestClient, self).post(*args, **kwargs)

    def put(self, *args, **kwargs):
        kwargs = self._enforce_request_format(kwargs)
        kwargs = self._stringify_data(kwargs)

        return super(TestClient, self).put(*args, **kwargs)


class TestCaseBase(TestCase):
    """Base class for unit tests, to store common methods and properties."""

    api_client = TestClient()
