import argparse
from pathlib import Path
from typing import Dict

CONFIG: Dict = {
    'general': {
        'internal_backend_port': 8000,  # Django default server port
        'interal_psql_port': 5432,  # PostGreSQL default port
        'internal_db_admin_port': 5050  # PGAdmin default port
    },
    'develop': {
        'requirements_file': 'test_requirements.txt',
        'external_backed_port': 8000,
        'external_psql_port': 5555,
        'external_db_admin_port': 5050
    },
    'staging': {
        'requirements_file': 'test_requirements.txt',
        'external_backed_port': 8001,
        'external_psql_port': 5556,
        'external_db_admin_port': 5051
    }
}


def parse_args():
    parser = argparse.ArgumentParser(description='Replace variables in docker configuration.')
    parser.add_argument('environment', type=str, help='Environment: can be "develop" or "staging"')

    return parser.parse_args()


if __name__ == '__main__':
    """Create the Docker compose yaml file based on the chosen environment"""

    arguments = parse_args()

    config = dict(CONFIG['general'])  # Create copy to modify
    config.update(CONFIG[arguments.environment])

    configuration = Path('docker-compose.tpl').read_text()
    for tag, value in config.items():
        configuration = configuration.replace('$$%s$$' % tag, str(value))

    Path('docker-compose.yml').write_text(configuration)
