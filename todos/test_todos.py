from datetime import datetime, timedelta
import uuid

import dateutil.parser
from dateutil.tz import tzlocal
from django.urls import reverse
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

from app_backend.unit_tests_base import TestCaseBase
from todos.models import Todo


class TodoTest(TestCaseBase):
    @staticmethod
    def insert_todos(count):
        """Insert an amount of todos with a random name and a start datetime of now."""

        new_todos = [
            Todo.objects.create(name=str(uuid.uuid4()), datetime_start=datetime.now(tzlocal()))
            for _counter in range(count)
        ]

        return new_todos

    def test_get_todos(self):
        """Tests the GET method of the Todos API."""

        new_todos = self.insert_todos(5)

        response = self.api_client.get(reverse('todos'))

        assert response.status_code == HTTP_200_OK

        todos = sorted(response.json(), key=lambda todo: todo['id'])

        # Compare serialized properties with objects
        for index, todo in enumerate(todos):
            assert new_todos[index].pk == todo['id']
            assert new_todos[index].name == todo['name']
            assert dateutil.parser.parse(todo['datetime_start']) == new_todos[index].datetime_start

    def test_post_todos(self):
        """Tests the POST method of the Todos API."""

        new_todo = {
            "name": "small leTTerS",
            "datetime_start": (datetime.now(tzlocal()) + timedelta(hours=1)).isoformat()
        }

        response = self.api_client.post(reverse('todos'), data=new_todo)

        assert response.status_code == HTTP_200_OK
        data = response.data

        assert 'id' in data
        assert data['datetime_start'] == new_todo['datetime_start']
        assert data['name'] == 'Small leTTerS'  # Only first char is capitalized
        assert not data['datetime_end']
        assert len(data) == 4  # No properties other than specified

        assert Todo.objects.filter(pk=data['id']).count()  # Assert creation in database

    def test_post_todos_missing_property(self):
        """Tests all required properties of the POST method of the Todos API."""

        missing_datetime_start = {"name": "new todo"}
        missing_datetime_end = {
            "name": "new todo",
            "datetime_start": (datetime.now(tzlocal()) + timedelta(hours=1)).isoformat()
        }
        missing_name = {"datetime_start": (datetime.now(tzlocal()) + timedelta(hours=1)).isoformat()}

        response = self.api_client.post(reverse('todos'), data=missing_name)
        assert response.status_code == HTTP_400_BAD_REQUEST
        assert 'name' in response.data
        assert response.data['name'][0].code == 'required'

        response = self.api_client.post(reverse('todos'), data=missing_datetime_start)
        assert response.status_code == HTTP_400_BAD_REQUEST
        assert 'datetime_start' in response.data
        assert response.data['datetime_start'][0].code == 'required'

        response = self.api_client.post(reverse('todos'), data=missing_datetime_end)
        assert response.status_code == HTTP_200_OK

    def test_post_todos_invalid_datetime_start(self):
        """Tests that the datetime_start should be in the future, when using the POST of the Todos API."""

        invalid_start_date_todo = {
            "name": "new todo", "datetime_start": (datetime.now(tzlocal()) - timedelta(hours=1)).isoformat()
        }

        response = self.api_client.post(reverse('todos'), data=invalid_start_date_todo)

        assert response.status_code == HTTP_400_BAD_REQUEST
        assert 'datetime_start' in response.data
        assert response.data['datetime_start'][0].code == 'invalid'

    def test_put_todos(self):
        """Tests the PUT method of the Todos API."""

        new_todo = self.insert_todos(1)[0]

        change_dict = {
            'pk': new_todo.pk,
            'name': new_todo.name + 'changed'
        }

        response = self.api_client.put(reverse('todos'), data=change_dict)

        assert response.status_code == HTTP_200_OK
        data = response.data

        # Assert database change
        changed_record = Todo.objects.get(pk=new_todo.pk)
        assert changed_record.name == change_dict['name']
        assert changed_record.datetime_start == new_todo.datetime_start
        assert changed_record.datetime_end == new_todo.datetime_end
        assert changed_record.id == new_todo.pk

        # Test API output
        assert data['name'] == change_dict['name']
        assert dateutil.parser.parse(data['datetime_start']) == new_todo.datetime_start
        assert data['datetime_end'] == new_todo.datetime_end
        assert data['id'] == new_todo.pk
