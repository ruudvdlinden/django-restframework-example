from datetime import datetime

from dateutil.tz import tzutc
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from todos.models import Todo


class TodoSerializer(serializers.ModelSerializer):
    def validate_datetime_start(self, value):
        if value < datetime.now(tzutc()):
            raise ValidationError("Start datetime should be in the future")

    class Meta:
        model = Todo
        fields = ('id', 'name', 'datetime_start', 'datetime_end')
