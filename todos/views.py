from django.db import transaction
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from rest_framework.response import Response

from todos.models import Todo
from todos.serializers import TodoSerializer


class TodosList(APIView):
    serializer = TodoSerializer

    def get(self, request):
        todos = list(Todo.objects.all())

        serialized = self.serializer(todos, many=True)

        return Response(serialized.data)

    @transaction.atomic
    def post(self, request):
        """Creates a new todo with the supplied data."""

        serialized = self.serializer(data=request.data)

        if serialized.is_valid():
            # Capitalizes first letter of name
            request.data['name'] = request.data['name'][0].capitalize() + request.data['name'][1:]

            new_todo = Todo.objects.create(**request.data)
            return_data, status_code = self.serializer(new_todo).data, HTTP_200_OK
        else:
            return_data, status_code = serialized.errors, HTTP_400_BAD_REQUEST

        return Response(return_data, status=status_code)

    @transaction.atomic
    def put(self, request):
        """
        At the moment, this method is purely to facilitate dragging Todos to another date. If it's used to edit other
        properties in other ways, it needs to be expanded with checking validity of input data.
        """

        data = request.data

        todo = Todo.objects.filter(pk=int(data.get('pk') or data.get('id')))

        todo.update(**{prop: value for prop, value in data.items() if prop not in ['id', 'pk']})

        serialized = self.serializer(todo[0], many=False)

        return Response(serialized.data)
