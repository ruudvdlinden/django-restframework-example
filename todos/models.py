from django.db import models


class Todo(models.Model):
    name = models.CharField(max_length=128)
    datetime_start = models.DateTimeField()
    datetime_end = models.DateTimeField(null=True)
